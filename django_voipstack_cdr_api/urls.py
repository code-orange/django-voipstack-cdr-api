from django.urls import path, include
from tastypie.api import Api

from django_voipstack_cdr_api.django_voipstack_cdr_api.api.resources import *

v1_api = Api(api_name="v1")
v1_api.register(CallsResource())
v1_api.register(CallsByRangeResource())
v1_api.register(CrmResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
