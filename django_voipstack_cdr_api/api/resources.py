from collections import OrderedDict
from datetime import timedelta

from django.contrib.auth.models import User
from django.db.models import signals
from django.forms.models import model_to_dict
from tastypie.authentication import ApiKeyAuthentication
from tastypie.models import create_api_key
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_voipstack_cdr_data.django_voipstack_cdr_data.models import *
from django_ispstack_pubapi_models.django_ispstack_pubapi_models.models import *

signals.post_save.connect(create_api_key, sender=User)


class CallsResource(Resource):
    class Meta:
        queryset = VoipCdr.objects.all()
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiKeyAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        cdr_id = str(kwargs["pk"])

        if not cdr_id.isdigit():
            # wrong format
            return

        # get users trunks
        customer_list = list()

        for pubapi_customer in PubApiCustomerUsers.objects.using("default").filter(
            user=bundle.request.user
        ):
            customer_list.append(pubapi_customer.customer)

        customer_trunks = VoipTrunks.objects.filter(customer_id__in=customer_list)

        bundle.obj = VoipCdr.objects.filter(
            id=cdr_id,
            trunk__in=customer_trunks,
        ).order_by("call_start")

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = OrderedDict()

            for item in bundle.obj.obj:
                bundle.data[item.id] = model_to_dict(
                    item,
                    fields=(
                        "call_start",
                        "call_end",
                        "call_answer",
                        "call_dispo",
                        "call_duration",
                        "call_billsec",
                        "import_flag",
                    ),
                )

                bundle.data[item.id]["num_src"] = "+" + str(item.num_src)
                bundle.data[item.id]["num_dst"] = "+" + str(item.num_dst)

                if item.account == item.trunk.trunk_name:
                    bundle.data[item.id]["direction"] = "out"
                else:
                    bundle.data[item.id]["direction"] = "in"

                bundle.data[item.id]["trunk"] = item.trunk.trunk_name

        return bundle


class CallsByRangeResource(CallsResource):
    def obj_get(self, bundle, **kwargs):
        id = str(kwargs["pk"])

        if not id.isdigit():
            # wrong format
            return

        id_len = len(id)

        # check for date resolution
        if id_len == 4:
            # year
            cdr_from = datetime.strptime(id, "%Y")
            cdr_to = datetime.strptime(str(int(id) + 1), "%Y")
        elif id_len == 6:
            # month
            cdr_from = datetime.strptime(id, "%Y%m")

            tmp_year = id[0:4]
            tmp_month = id[4:7]

            if (int(tmp_month) + 1) > 12:
                tmp_year = str(int(tmp_year) + 1)
                tmp_month = "01"

            cdr_to = datetime.strptime(tmp_year + tmp_month, "%Y%m")
        elif id_len == 8:
            # day
            cdr_from = datetime.strptime(id, "%Y%m%d")
            cdr_to = cdr_from + timedelta(days=1)
        else:
            # wrong format
            return

        # get users trunks
        customer_list = list()

        for pubapi_customer in PubApiCustomerUsers.objects.using("default").filter(
            user=bundle.request.user
        ):
            customer_list.append(pubapi_customer.customer)

        customer_trunks = VoipTrunks.objects.filter(customer_id__in=customer_list)

        bundle.obj = VoipCdr.objects.filter(
            call_start__gte=cdr_from,
            call_end__lte=cdr_to,
            call_dispo="ANSWERED",
            trunk__in=customer_trunks,
        ).order_by("call_start")

        return bundle


class CrmResource(CallsResource):
    class Meta(CallsResource.Meta):
        allowed_methods = ["get", "post"]

    def obj_get(self, bundle, **kwargs):
        trunk_name = str(kwargs["pk"])

        # get users trunks
        customer_list = list()

        for pubapi_customer in PubApiCustomerUsers.objects.using("default").filter(
            user=bundle.request.user
        ):
            customer_list.append(pubapi_customer.customer)

        customer_trunks = VoipTrunks.objects.filter(
            customer_id__in=customer_list, trunk_name=trunk_name
        )

        bundle.obj = VoipCdr.objects.filter(
            call_dispo="ANSWERED",
            trunk__in=customer_trunks,
            import_flag=0,
        ).order_by("call_start")

        return bundle

    def obj_create(self, bundle, **kwargs):
        bundle.related_obj = None
        bundle.obj = None

        # get users trunks
        customer_list = list()

        for pubapi_customer in PubApiCustomerUsers.objects.using("default").filter(
            user=bundle.request.user
        ):
            customer_list.append(pubapi_customer.customer)

        customer_trunks = VoipTrunks.objects.filter(customer_id__in=customer_list)

        try:
            cdr_entry = VoipCdr.objects.get(
                id=bundle.data["call_id"], trunk__in=customer_trunks
            )
        except VoipCdr.DoesNotExist:
            return bundle

        bundle.related_obj = cdr_entry
        bundle.obj = cdr_entry

        bundle.obj.import_flag = 1
        bundle.obj.save()

        return bundle
